/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmiharjoitus;

import java.nio.file.Paths;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author debian1
 */
public class SortComponent{

    private Sorter sorter;
    private SorterName name;
    
    private int size = 100;
    private int repeat = 1;
    private int step = 1;
    private int random = 100;
    private int sortCount;

    private double average;
    private double timeSum;

    private Order order = Order.RANDOM;
    private final String projPath = Paths.get("").toAbsolutePath().toString();

    private final TextField tfStep = new TextField("1");
    private final TextField tfRepeat = new TextField("1");
    private final TextField tfArraySize =  new TextField("100");
    private final TextField tfAvg = new TextField();
    private final TextField tfCounter = new TextField();

    private final Label lblRepeat = new Label("repeat");
    private final Label lblStep = new Label("Step");
    private final Label lblTitle = new Label();
    
    private final Button btnSort = new Button("sort");
    
    
    private final ListView<Result> resultsList = new ListView<>();
    private final ContextMenu contextMenu = new ContextMenu();
    private final MenuItem clearItem = new MenuItem("Clear results");
    private final MenuItem randomItem = new MenuItem("random");
    private final MenuItem decreasingItem = new MenuItem("descending");
    private final MenuItem increasingItem = new MenuItem("increasing");
    private final MenuItem graphItem = new MenuItem("look at graph");


    {//init block
        contextMenu.setOnShowing((WindowEvent e) -> {
            System.out.println("showing");
        });
 
        clearItem.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                clearAll();
            }
        });
        
        randomItem.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                order = Order.RANDOM;
            }
        });
        
        decreasingItem.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                order = Order.DEC;
            }
        });
        
        increasingItem.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                order = Order.INC;
            }
        });
        
        graphItem.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Stage s = getGraph();
                s.show();
            }
        });
        contextMenu.getItems().addAll(
                randomItem,increasingItem,decreasingItem,graphItem,clearItem);
        
        resultsList.setContextMenu(contextMenu);
        
        lblTitle.rotateProperty().set(90);
        
        tfArraySize.autosize();
        tfArraySize.setMaxWidth(100);

        tfArraySize.textProperty().addListener((observable,oldValue,newValue) -> {
            if(!newValue.equals("")){
                this.size = Integer.parseInt(tfArraySize.textProperty().getValue());
            }
        });

        lblRepeat.autosize();
        tfRepeat.autosize();
        lblRepeat.setMaxWidth(100);
        tfRepeat.setMaxWidth(100);
        

        tfRepeat.textProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue.equals("")){
                this.repeat = Integer.parseInt(newValue);
            }
        });

        lblStep.setPrefWidth(60);
        tfStep.setMaxWidth(100);

        tfStep.textProperty().addListener((observable, oldValue, newValue) -> {
            
            if(!newValue.equals("")){
                this.step = Integer.parseInt(newValue);
            }
        });
        tfAvg.autosize();
        tfAvg.setMaxWidth(100);
        tfAvg.setEditable(false);
        tfAvg.textProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue.equals("")){
                this.average = Double.parseDouble(newValue);
            }
        });
        tfCounter.autosize();
        tfCounter.setMaxWidth(100);

        btnSort.autosize();
        btnSort.setPrefHeight(50);
        btnSort.setMaxWidth(100);

        btnSort.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                sorter.initForSearch(size, order, repeat, step, random);
                //??
                Thread t = new Thread(sorter);
                t.start();
                try{
                    t.join(0);
                }catch(InterruptedException ie){
                    System.out.println("thread err...");
                }
                //??
                
                sorter.results.forEach((result) -> {
                    timeSum += result.runtime;
                    ++sortCount;
                });
                
                average = timeSum/sortCount;
                tfCounter.textProperty().setValue(String.valueOf(sorter.results.size()));
                tfAvg.textProperty().setValue(String.format("%.3f",average));
                tfArraySize.textProperty().setValue(String.valueOf(size + repeat*step));
                }
            
        });

    }//init field end
    
    public SortComponent(SorterName a) {
        this.name = a;
        this.sorter = new Sorter(name);
        lblTitle.textProperty().setValue(name.name().toLowerCase());
        resultsList.setMaxHeight(170);
        resultsList.setMaxWidth(270);
        resultsList.autosize();
        resultsList.autosize();

        resultsList.setItems(sorter.results);
    }

    public void clearAll() {
        tfAvg.textProperty().setValue("0");
        sorter.results.clear();
        sortCount = 0;
        tfCounter.textProperty().setValue("0");
        tfArraySize.textProperty().setValue("100");
    }

    private final Label lblCounter = new Label("counter");
    private final Label lblArraySize = new Label("size");
    private final Label lblAvg = new Label("average");

    private VBox rightControls() {
        VBox result = new VBox();
        result.getChildren().addAll(lblRepeat, tfRepeat, lblStep, 
                tfStep,lblCounter,tfCounter);
        result.setSpacing(2);
        return result;
    }

    private VBox leftControls() {
        VBox result = new VBox();
        result.getChildren().addAll(lblArraySize,tfArraySize,
                lblAvg,tfAvg,btnSort);
        result.setSpacing(2);
        
        return result;

    }

    public VBox Box() {

        VBox result = new VBox();

        BorderPane sections = new BorderPane();
        sections.setCenter(lblTitle);
        sections.setLeft(leftControls());
        sections.setRight(rightControls());
        sections.setBottom(resultsList);
        
        sections.isVisible();
        sections.setPadding(new Insets(5,5,5,5));
        result.getChildren().addAll(sections);
        result.setPadding(new Insets(2,2,2,2));
        VBox.setMargin(result, new Insets(5, 5, 5, 5));

        return result;
    }

    public Stage getGraph() {
        Stage stage = new Stage();
        stage.setTitle(name+" Scatter Chart");

        XYChart.Series series1 = new XYChart.Series();
        series1.setName(name.name());

        int maxItems = 0;
        double longestRun = 0;

        for (int i = 0; i < resultsList.getItems().size(); i++) {

            maxItems = resultsList.getItems().get(i).count > maxItems
                    ? resultsList.getItems().get(i).count : maxItems;

            longestRun = resultsList.getItems().get(i).runtime > longestRun
                    ? resultsList.getItems().get(i).runtime : longestRun;

            series1.getData().add(new XYChart.Data(
                    resultsList.getItems().get(i).count,
                    resultsList.getItems().get(i).runtime));
        }

        final NumberAxis xAxis = new NumberAxis(0, maxItems, 100);
        final NumberAxis yAxis = new NumberAxis(0, longestRun, 1);

        final ScatterChart<Number, Number> sc = new ScatterChart<>(xAxis, yAxis);

        xAxis.setLabel("Items");
        yAxis.setLabel("milliseconds");

        sc.setTitle("performance");
        sc.getData().addAll(series1);
        sc.setPrefSize(1000, 800);
        
        Scene scene = new Scene(sc, 1000, 800);
        scene.getStylesheets().add(getClass().getResource("styles.css").toExternalForm());
        
        sc.getStylesheets().add(projPath + "/src/algoritmiharjoitus/styles.css");
        
        stage.setScene(scene);
        
        return stage;
    }
    
    public void setSize(int size) {
        this.size = size;
        tfArraySize.textProperty().setValue(String.valueOf(size));
    }

    public void setStep(int step) {
        this.step = step;
        tfStep.textProperty().setValue(String.valueOf(step));
    }

    public void setRepeat(int repeat) {
        this.repeat = repeat;
        tfRepeat.textProperty().setValue(String.valueOf(repeat));
    }
    public void setRandom(int random){
        this.random = random;
    }

    public String getName() {
        return this.name.name().toLowerCase();
    }
    
}