/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmiharjoitus;

/**
 *
 * @author debian1
 */
public class InsertionSort implements ISort {
    public int opCount;
   
    @Override
    public long sort(int[] array) {
        long start = System.nanoTime();
        for (int i = 1; i < array.length; i++) {/*n-1 compares, n-1 increments*/
            int valueToSort = array[i]; /*n assignments*/
            int j = i;                  /*n assignments*/
                while (j > 0 && array[j - 1] > valueToSort) { 
                        array[j] = array[j - 1]; 
                        j--;
                }
                array[j] = valueToSort;
        }
        long stop = System.nanoTime();
        return (stop-start);
    }


}
