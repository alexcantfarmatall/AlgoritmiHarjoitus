/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmiharjoitus;

/**
 *
 * @author debian1
 */
public class BubbleSort implements ISort {
    @Override
    public long sort(int [ ] array )
    {
     int j;
     boolean flag = true;   // set flag to true to begin first pass
     int temp;   //holding variable
     long start = System.nanoTime();
     while (flag)
     {
            flag= false;    //set flag to false awaiting a possible swap
            for( j=0;  j < array.length -1;  j++ )
            {
                   if ( array[ j ] > array[j+1] )   // change to > for ascending sort
                   {
                           temp = array[ j ];                //swap elements
                           array[ j ] = array[ j+1 ];
                           array[ j+1 ] = temp;
                          flag = true;              //shows a swap occurred 
                  }
            }
      }
     long stop = System.nanoTime();
     for(int i : array){
         System.out.println("bubble sorted: " +i);
     }
     return (stop-start);
}
 
    
}
