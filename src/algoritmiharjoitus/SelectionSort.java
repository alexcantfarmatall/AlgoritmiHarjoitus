/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmiharjoitus;

/**
 *
 * @author debian1
 */
public class SelectionSort implements ISort{
    @Override
    public long sort(int[] array){
        int i, j, first, temp;
        long opsCount = 0;
        long start = System.nanoTime();
        for (i = array.length - 1; i > 0; i-- )
        {
            first = 0;   //initialize to subscript of first element
            for (j = 1; j <= i; j++)   //locate smallest element between positions 1 and i.
            {
                opsCount++;
                if (array[j] > array[first])
                    first = j;
            }
            temp = array[first];   //swap smallest found with element in position i.
            array[first] = array[i];
            array[i] = temp;
        }
        long stop = System.nanoTime();
        System.out.println("SELECTION OPERATIONS COUNT: " +opsCount);

        return (stop-start);
    }

}
