/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmiharjoitus;

/**
 *
 * @author debian1
 */
public class ShellSort implements ISort{
    @Override
    public long sort(int[] array) {
    int inner, outer;
    int temp;
 
    int h = 1;
    
    long start = System.nanoTime();
    while (h <= array.length / 3) {
      h = h * 3 + 1;
    }
    while (h > 0) {
      for (outer = h; outer < array.length; outer++) {
        temp = array[outer];
        inner = outer;
 
        while (inner > h - 1 && array[inner - h] >= temp) {
          array[inner] = array[inner - h];
          inner -= h;
        }
        array[inner] = temp;
      }
      h = (h - 1) / 3;
    }
    long stop = System.nanoTime();
    
    return (stop-start);
  }
    
}
