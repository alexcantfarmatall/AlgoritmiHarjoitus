/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmiharjoitus;

import java.util.Arrays;
import java.util.Collections;

/**
 *
 * @author debian1
 */
public class JavaSort implements ISort{
  

    @Override
    public long sort(int[] array) {
        
        long start = System.nanoTime();
        Arrays.sort(array);
        long stop = System.nanoTime();
        
        return stop - start;
    }
    
}
