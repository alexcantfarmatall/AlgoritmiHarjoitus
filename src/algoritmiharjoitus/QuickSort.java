/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmiharjoitus;

import java.util.Arrays;

/**
 *
 * @author debian1
 */
public class QuickSort implements ISort {
    private int[] numbers;
    private int number;
    private long opsCount = 0;
    @Override
    public long sort(int[] array) {
        // check for empty or null array

        long start = System.nanoTime();

        if (array ==null || array.length==0){
            return -1;
        }
        this.numbers = array;
        number = array.length;
        
        quicksort(0, number - 1);
        long stop = System.nanoTime();
        for(int i : array){
            System.out.println("quick: " + i);
        }

        return (stop-start);
    }

    private void quicksort(int low, int high) {
        int i = low, j = high;
        // Get the pivot element from the middle of the list
        int pivot = numbers[low + (high-low)/2];

        // Divide into two lists
        while (i <= j) {
            // If the current value from the left list is smaller than the pivot
            // element then get the next element from the left list
            //opsCount++;
            while (numbers[i] < pivot) {
                i++;
                opsCount++;
            }
            // If the current value from the right list is larger than the pivot
            // element then get the next element from the right list
            while (numbers[j] > pivot) {
                opsCount++;
                j--;
            }

            // If we have found a value in the left list which is larger than
            // the pivot element and if we have found a value in the right list
            // which is smaller than the pivot element then we exchange the
            // values.
            // As we are done we can increase i and j
            if (i <= j) {
                exchange(i, j);
                i++;
                j--;
            }
        }
        // Recursion
        if (low < j)
            quicksort(low, j);
        if (i < high)
            quicksort(i, high);
    }

    private void exchange(int i, int j) {
        int temp = numbers[i];
        numbers[i] = numbers[j];
        numbers[j] = temp;
    }
}
