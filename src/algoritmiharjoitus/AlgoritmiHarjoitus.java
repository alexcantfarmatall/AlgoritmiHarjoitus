/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmiharjoitus;

import java.util.HashMap;
import java.util.Map;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author debian1
 */
public class AlgoritmiHarjoitus extends Application {

    //private Sorter sorter;
    private HashMap<SorterName, SortComponent> sorters;
    
    private int allRepeat;
    private int allStep;
    private int arraySize;
    private int randomBound = 1000;
    
    private final TextField tfArraySize = new TextField();
    private final TextField tfRandomBound = new TextField("100");
    private final TextField tfAllRepeat = new TextField();
    private final TextField tfAllStep = new TextField();
    private final Button btnClearAll = new Button("clear all");
   
    {//init block

        btnClearAll.setOnAction(e->{
           sorters.forEach((k,v)->{
               v.clearAll();
        });
        });
        btnClearAll.autosize();
        tfRandomBound.textProperty().addListener((cl, oldValue, newValue)->{
            if(!newValue.equals("")){
                this.randomBound = Integer.parseInt(newValue);
            }else{
                this.randomBound = 100;
            }
            sorters.forEach((k,v)->{
               v.setRandom(randomBound);
            });
            
        });
        
        tfAllRepeat.textProperty().addListener((cl, oldValue, newValue)->{
            if(!newValue.equals("")){
                this.allRepeat = Integer.parseInt(newValue);
            }else{
                this.allRepeat = 1;
            }
            sorters.forEach((k,v)->{
               v.setRepeat(allRepeat);
            });
            
        });
        
         tfAllStep.textProperty().addListener((cl, oldValue, newValue)->{
            if(!newValue.equals("")){
                this.allStep = Integer.parseInt(newValue);
            }else{
                this.allStep = 0;
            }
            sorters.forEach((k,v)->{
               v.setStep(allStep);
            });
        });
         
       
        tfArraySize.textProperty().addListener((cl, oldValue, newValue)->{
            if(!newValue.equals("")){
                this.arraySize = Integer.parseInt(newValue);
            }else{
                this.arraySize = 100;
            }
            sorters.forEach((k,v)->{
                v.setSize(arraySize);
            });
        });    
    }//init field end
    
    private TilePane globalControls() {
        TilePane result = new TilePane();
        result.setPrefColumns(1);
        
        VBox vbArraySize = new VBox();
        final Label lblArraySize = new Label("Array Size");
        vbArraySize.getChildren().addAll(lblArraySize,tfArraySize);
        vbArraySize.setSpacing(2);
        vbArraySize.setPadding(new Insets(2));

        VBox vbRepeat = new VBox();
        final Label lblAllRepeat = new Label("Repeat");
        vbRepeat.getChildren().addAll(lblAllRepeat,tfAllRepeat);
        vbRepeat.setSpacing(2);
        vbRepeat.setPadding(new Insets(2));
        
        VBox vbStep = new VBox();
        final Label lblAllStep = new Label("Increment by");
        vbStep.getChildren().addAll(lblAllStep,tfAllStep);
        vbStep.setSpacing(2);
        vbStep.setPadding(new Insets(2));


        VBox vbRandomBoundAndClear = new VBox();
        final Label lblRandomBound = new Label("Random upper Bound");
        vbRandomBoundAndClear.getChildren().addAll(
                lblRandomBound,tfRandomBound, btnClearAll);
        vbRandomBoundAndClear.setSpacing(2);
        vbRandomBoundAndClear.setPadding(new Insets(2));
        
        result.setAlignment(Pos.BASELINE_LEFT);
        result.setPadding(new Insets(5));
        result.getChildren().addAll(
                vbArraySize,vbRepeat,vbStep,vbRandomBoundAndClear);
        
        result.getChildren().forEach((n) -> {
            TilePane.setMargin(n, new Insets(2));
        });
        return result;
    }

    private BorderPane sortPane() {
        TilePane sortComponents = new TilePane();
       
        sortComponents.setPrefColumns(4);
        sortComponents.setPrefRows(2);
        sortComponents.setPadding(new Insets(5,5,5,5));
        sortComponents.autosize();
        sortComponents.setHgap(5);
        sortComponents.setVgap(5);
        
        for(Map.Entry<SorterName,SortComponent> entry : sorters.entrySet()){
            sortComponents.getChildren().add(entry.getValue().Box());
        }
        
        BorderPane result = new BorderPane();
        result.setCenter(sortComponents);
        result.setRight(globalControls());
        result.setPadding(new Insets(5, 5, 5, 5));

        return result;
    }

    

    

    @Override
    public void start(Stage primaryStage) {

        //sorter = new Sorter();
        sorters = new HashMap<SorterName, SortComponent>();

        sorters.put(SorterName.BUBBLE, new SortComponent(SorterName.BUBBLE));
        sorters.put(SorterName.INSERTION, new SortComponent(SorterName.INSERTION));
        sorters.put(SorterName.BININSERTION, new SortComponent(SorterName.BININSERTION));
        sorters.put(SorterName.MERGE, new SortComponent(SorterName.MERGE));
        sorters.put(SorterName.QUICK, new SortComponent(SorterName.QUICK));
        sorters.put(SorterName.SELECTION, new SortComponent(SorterName.SELECTION));
        sorters.put(SorterName.SHELL, new SortComponent(SorterName.SHELL));
        sorters.put(SorterName.JAVA, new SortComponent(SorterName.JAVA));

        
        Group root = new Group();
        root.getChildren().addAll(sortPane());
        final Scene scene = new Scene(root);

        primaryStage.setTitle("algs");
        primaryStage.setScene(scene);

        //primaryStage.setFullScreen(true);
        /*
        System.out.println("before sceneW " + scene.getWidth());
        System.out.println("before sceneH " +  scene.getHeight());
        System.out.println("before stageW " + primaryStage.getWidth());
        System.out.println("before stageH " + primaryStage.getHeight());
         */
        primaryStage.show();
        /*
        System.out.println("after sceneW " + scene.getWidth());
        System.out.println("after sceneH " +  scene.getHeight());
        System.out.println("after stageW " + primaryStage.getWidth());
        System.out.println("after stageH " + primaryStage.getHeight());
         */
        primaryStage.setMinWidth(primaryStage.getWidth());
        primaryStage.setMaxWidth(primaryStage.getWidth());
        primaryStage.setMinHeight(primaryStage.getHeight() + 50);
        primaryStage.setMaxHeight(primaryStage.getHeight() + 50);

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
