/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmiharjoitus;

import java.util.Random;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author debian1
 */
public class Sorter extends Thread{
    public ObservableList<Result> results;
    private Order order;

    private int itemBound = 100;
    private long sortCounter;
    private int repeat = 1;
    private int step;
    private int sizeOfArray = 100;
    public int[] array;
    private SorterName name;
    private ISort alg;

    public void setItemBound(int bound){
        if(bound > 1000000){
            System.out.println("...might take a while");
        }
        this.itemBound = bound;
    }
    
    public Sorter(SorterName a){
        this.name = a;
        results = FXCollections.observableArrayList();
        switch(name){
            case BININSERTION:{this. alg = new BinaryInsertionSort();System.out.println("binsertion");break;}
            case INSERTION:{this. alg = new InsertionSort();System.out.println("insertion");break;}
            case MERGE:{this.alg = new MergeSort();System.out.println("merge");break;}
            case BUBBLE:{this.alg = new BubbleSort();System.out.println("bubble");break;}
            case QUICK:{this.alg = new QuickSort();System.out.println("quick");break;}
            case SELECTION:{this.alg = new SelectionSort();System.out.println("selection");break;}
            case SHELL:{this.alg = new ShellSort();System.out.println("shell");break;}
            case JAVA:{this.alg = new JavaSort();System.out.println("java");break;}
            case HEAP:{this.alg = new HeapSort();System.out.println("heap");break;}
            default:{System.out.println("java");this.alg = new JavaSort();break;}
        }
    }

    public void initForSearch(
    int sizeOfArray, Order sortOrder,int repeat, int step, int random){
        
        this.sizeOfArray = sizeOfArray;
        this.repeat = repeat;
        this.step   = step;
        this.array  = new int[sizeOfArray];

        switch (sortOrder) {
            
            case INC:{
                order = Order.INC;
                fill();
                break;
            }
            
            case DEC:{
                order = Order.DEC;
                reverseFill();
                break;
            }
            
            default:{
                order = Order.RANDOM;
                randomFill();
                break;
            }
        }
        
    }
    @Override
    public void run()
    {
        System.out.println("before sort");
        for (int i = 0; i < repeat; i++) {
                
            sortCounter++;
            sizeOfArray += step;
            
        long result = alg.sort(array);
        System.out.println("at run()... " + result);
        results.add(new Result(result, array.length, order));
        initForSearch(sizeOfArray, order,repeat, step, itemBound);
        }
        
    }

    private void reverseFill(){
        int j = 0;
        for(int i = (array.length - 1); i >= 0; i--){
            array[i] = j++;
        }
    }

    private void randomFill(){
        Random rnd = new Random();
        for(int i = 0; i < array.length; i++){
            array[i] = rnd.nextInt(itemBound);
        }
    }
    private void fill(){
        for(int i = 0; i < array.length; i++){
            array[i] = i;
        }
    }

    public void setBound(int bound){
        itemBound = bound;
    }
}
