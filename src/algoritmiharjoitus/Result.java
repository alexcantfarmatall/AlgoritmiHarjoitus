/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmiharjoitus;

/**
 *
 * @author debian1
 */
public class Result {
    double runtime;
    int count;
    private Order order;

    public Result(long runtime, int count, Order order){
        this.runtime = runtime/1000000.0;
        this.count = count;
        this.order = order;
    }

    @Override
    public String toString(){
        return this.runtime + "ms [x" + this.count + "]["+this.order+"]";
    }

}
