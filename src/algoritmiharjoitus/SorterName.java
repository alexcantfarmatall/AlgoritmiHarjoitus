/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmiharjoitus;

/**
 *
 * @author deb
 */
public enum SorterName {
    BUBBLE,
    HEAP,
    INSERTION,
    BININSERTION,
    JAVA,
    MERGE,
    QUICK,
    SELECTION,
    SHELL;
    
}
