/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmiharjoitus;

import java.util.Arrays;

/**
 *
 * @author deb
 */
class BinaryInsertionSort implements ISort{

    
	
    @Override
    public long sort(int array[]) { 
        long start = System.nanoTime();
        for (int i = 1; i < array.length; i++) {
        	   int x = array[i];
        	   
        	   // Find location to insert using binary search
        	   int j = Math.abs(Arrays.binarySearch(array, 0, i, x) + 1);
        	   
        	   //Shifting array to one location right
        	   System.arraycopy(array, j, array, j+1, i-j);
        	   
        	   //Placing element at its correct location
        	   array[j] = x;
        }
        long stop = System.nanoTime();
        for(int i : array){
            System.out.println("binsertion: " + i);
        }
        return stop-start;
    }
}

